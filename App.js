/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { ListItem, Divider } from 'react-native-elements';

type Props = {};
export default class App extends Component<Props> {
  state = {
    data: [{ id: "1" }]
  }

  renderItem = () => (
    <View>
      <ListItem
        roundAvatar
        rightIcon={{ name: 'chevron-right',type: 'MaterialIcon'}}
        title={'Tuhin Hossain'}
        subtitle={'Software Engineer'}
        leftAvatar={{ title: 'Tuhin Hossain', source: { uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"} }}
      />
      <Divider style={{ backgroundColor: 'slategray' }} />
    </View>
  );

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          keyExtractor={(value) => value.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
